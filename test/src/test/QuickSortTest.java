package test;

import data_structures.ILista;
import data_structures.ListaEncadenada;
import junit.framework.TestCase;
import model.logic.QuickSort;
import model.vo.VOPelicula;

public class QuickSortTest extends TestCase{
	
	private QuickSort quick = new QuickSort();
	
	private VOPelicula[] a;
	
	private VOPelicula p1 = new VOPelicula();
	private VOPelicula p2 = new VOPelicula();
	private VOPelicula p3 = new VOPelicula();
	private VOPelicula p4 = new VOPelicula();
	private VOPelicula p5 = new VOPelicula();
	
	private void crearPeliculas()
	{
		ILista<String> genero1 = new ListaEncadenada<String>();
		genero1.agregarElementoFinal("Drama");
		genero1.agregarElementoFinal("Policiaco");

		p1.setAgnoPublicacion(2004);
		p1.setTitulo("Crash");
		p1.setGenerosAsociados(genero1);

		ILista<String> genero2 = new ListaEncadenada<String>();
		genero2.agregarElementoFinal("Comedia");
		genero2.agregarElementoFinal("Drama");

		p2.setAgnoPublicacion(2007);
		p2.setTitulo("Little Miss Sunshine");
		p2.setGenerosAsociados(genero2);

		ILista<String> genero3 = new ListaEncadenada<String>();
		genero3.agregarElementoFinal("Gansters");


		p3.setAgnoPublicacion(1992);
		p3.setTitulo("Reservoir Dogs");
		p3.setGenerosAsociados(genero3);

		ILista<String> genero4 = new ListaEncadenada<String>();
		genero4.agregarElementoFinal("Ciencia ficcion");

		p4.setAgnoPublicacion(1977);
		p4.setTitulo("Star Wars");
		p4.setGenerosAsociados(genero4);

		ILista<String> genero5 = new ListaEncadenada<String>();
		genero5.agregarElementoFinal("Zombies");

		p5.setAgnoPublicacion(2013);
		p5.setTitulo("World War Z");
		p5.setGenerosAsociados(genero5);
		
		a = new VOPelicula[5];
	}
	
	private void crearAscendente()
	{
		a[0] = p1;
		a[1] = p2;
		a[2] = p3;
		a[3] = p4;
		a[4] = p5;
		
		quick.sort(a);
	}
	
	private void crearDescendente()
	{
		a[0] = p5;
		a[1] = p4;
		a[2] = p3;
		a[3] = p2;
		a[4] = p1;
		
		quick.sort(a);
	}

	private void crearAleatorio()
	{
		a[0] = p4;
		a[1] = p5;
		a[2] = p2;
		a[3] = p3;
		a[4] = p1;
		
		quick.sort(a);
	}
	
	public void testQuickSort()
	{
		crearPeliculas();
		
		crearAscendente();
		
		assertEquals("Deberia ser 'Crash'", "Crash", a[0].getTitulo());
		assertEquals("Deberia ser 'Little Miss Sunshine", "Little Miss Sunshine", a[1].getTitulo());
		assertEquals("Deberia ser 'Reservoir Dogs'", "Reservoir Dogs",a[2].getTitulo());
		assertEquals("Deberia ser 'Star Wars'", "Star Wars", a[3].getTitulo());
		assertEquals("Deberia ser 'World War Z'", "World War Z", a[4].getTitulo());
		
		crearDescendente();
		
		assertEquals("Deberia ser 'Crash'", "Crash", a[0].getTitulo());
		assertEquals("Deberia ser 'Little Miss Sunshine", "Little Miss Sunshine", a[1].getTitulo());
		assertEquals("Deberia ser 'Reservoir Dogs'", "Reservoir Dogs",a[2].getTitulo());
		assertEquals("Deberia ser 'Star Wars'", "Star Wars", a[3].getTitulo());
		assertEquals("Deberia ser 'World War Z'", "World War Z", a[4].getTitulo());
		
		crearAleatorio();
		
		assertEquals("Deberia ser 'Crash'", "Crash", a[0].getTitulo());
		assertEquals("Deberia ser 'Little Miss Sunshine", "Little Miss Sunshine", a[1].getTitulo());
		assertEquals("Deberia ser 'Reservoir Dogs'", "Reservoir Dogs",a[2].getTitulo());
		assertEquals("Deberia ser 'Star Wars'", "Star Wars", a[3].getTitulo());
		assertEquals("Deberia ser 'World War Z'", "World War Z", a[4].getTitulo());
	}
}
