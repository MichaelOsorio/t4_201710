package test;

import data_structures.ILista;
import data_structures.ListaEncadenada;
import junit.framework.TestCase;
import model.logic.QuickSortLista;
import model.vo.VOPelicula;

public class QuickSortListaTest extends TestCase{
	
	private QuickSortLista quick = new QuickSortLista();
	
	
	private ListaEncadenada<VOPelicula> lista1 = new ListaEncadenada<VOPelicula>();
	private ListaEncadenada<VOPelicula> lista2 = new ListaEncadenada<VOPelicula>();
	private ListaEncadenada<VOPelicula> lista3 = new ListaEncadenada<VOPelicula>();
	
	private VOPelicula p1 = new VOPelicula();
	private VOPelicula p2 = new VOPelicula();
	private VOPelicula p3 = new VOPelicula();
	private VOPelicula p4 = new VOPelicula();
	private VOPelicula p5 = new VOPelicula();
	
	private void crearPeliculas()
	{
		ILista<String> genero1 = new ListaEncadenada<String>();
		genero1.agregarElementoFinal("Drama");
		genero1.agregarElementoFinal("Policiaco");

		p1.setAgnoPublicacion(2004);
		p1.setTitulo("Crash");
		p1.setGenerosAsociados(genero1);

		ILista<String> genero2 = new ListaEncadenada<String>();
		genero2.agregarElementoFinal("Comedia");
		genero2.agregarElementoFinal("Drama");

		p2.setAgnoPublicacion(2007);
		p2.setTitulo("Little Miss Sunshine");
		p2.setGenerosAsociados(genero2);

		ILista<String> genero3 = new ListaEncadenada<String>();
		genero3.agregarElementoFinal("Gansters");


		p3.setAgnoPublicacion(1992);
		p3.setTitulo("Reservoir Dogs");
		p3.setGenerosAsociados(genero3);

		ILista<String> genero4 = new ListaEncadenada<String>();
		genero4.agregarElementoFinal("Ciencia ficcion");

		p4.setAgnoPublicacion(1977);
		p4.setTitulo("Star Wars");
		p4.setGenerosAsociados(genero4);

		ILista<String> genero5 = new ListaEncadenada<String>();
		genero5.agregarElementoFinal("Zombies");

		p5.setAgnoPublicacion(2013);
		p5.setTitulo("World War Z");
		p5.setGenerosAsociados(genero5);
		
		


	}
	
	private void  crearAscendente()
	{	
		lista1.agregarElementoFinal(p1);
		lista1.agregarElementoFinal(p2);
		lista1.agregarElementoFinal(p3);
		lista1.agregarElementoFinal(p4);
		lista1.agregarElementoFinal(p5);
		
		quick.sort(lista1);
	}

	private void crearDescendente()
	{
		lista2.agregarElementoFinal(p5);
		lista2.agregarElementoFinal(p4);
		lista2.agregarElementoFinal(p3);
		lista2.agregarElementoFinal(p2);
		lista2.agregarElementoFinal(p1);
		
		quick.sort(lista2);
	}

	private void crearAleatorio()
	{
		lista3.agregarElementoFinal(p2);
		lista3.agregarElementoFinal(p5);
		lista3.agregarElementoFinal(p3);
		lista3.agregarElementoFinal(p1);
		lista3.agregarElementoFinal(p4);
		
		quick.sort(lista3);
	}

	public void testQuickSortLista()
	{
		crearPeliculas();
		
		crearAscendente();
		
		assertEquals("Deberia ser 'Crash'", "Crash", lista1.darElemento(0).getTitulo());
		assertEquals("Deberia ser 'Little Miss Sunshine", "Little Miss Sunshine", lista1.darElemento(1).getTitulo());
		assertEquals("Deberia ser 'Reservoir Dogs'", "Reservoir Dogs",lista1.darElemento(2).getTitulo());
		assertEquals("Deberia ser 'Star Wars'", "Star Wars", lista1.darElemento(3).getTitulo());
		assertEquals("Deberia ser 'World War Z'", "World War Z", lista1.darElemento(4).getTitulo());
		
		crearDescendente();
		
		assertEquals("Deberia ser 'Crash'", "Crash", lista2.darElemento(0).getTitulo());
		assertEquals("Deberia ser 'Little Miss Sunshine", "Little Miss Sunshine", lista2.darElemento(1).getTitulo());
		assertEquals("Deberia ser 'Reservoir Dogs'", "Reservoir Dogs",lista2.darElemento(2).getTitulo());
		assertEquals("Deberia ser 'Star Wars'", "Star Wars", lista2.darElemento(3).getTitulo());
		assertEquals("Deberia ser 'World War Z'", "World War Z", lista2.darElemento(4).getTitulo());
		
		crearAleatorio();
		
		assertEquals("Deberia ser 'Crash'", "Crash", lista3.darElemento(0).getTitulo());
		assertEquals("Deberia ser 'Little Miss Sunshine", "Little Miss Sunshine", lista3.darElemento(1).getTitulo());
		assertEquals("Deberia ser 'Reservoir Dogs'", "Reservoir Dogs",lista3.darElemento(2).getTitulo());
		assertEquals("Deberia ser 'Star Wars'", "Star Wars", lista3.darElemento(3).getTitulo());
		assertEquals("Deberia ser 'World War Z'", "World War Z", lista3.darElemento(4).getTitulo());
		
		
	}
	
	
	

}
