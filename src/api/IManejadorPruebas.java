package api;

import model.vo.VOPelicula;

public interface IManejadorPruebas {
	
	public void cargarArchivoPeliculas(String archivoPeliculas);
	
	public void ordenarMuestraArreglo(int n);
	
	public void ordenarMuestraLista(int n);

}
