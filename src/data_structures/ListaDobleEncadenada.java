package data_structures;

import java.util.Iterator;

import data_structures.NodoDoble;

public class ListaDobleEncadenada<T> implements ILista<T> {

	private NodoDoble<T> primero;

	private NodoDoble<T> ultimo;

	private NodoDoble<T> actual;

	private int longitud;

	public ListaDobleEncadenada()
	{
		primero = null;
		ultimo = primero;
		actual = primero;
		longitud= 0;
	}

	public NodoDoble<T> darPrimero()
	{
		return primero;
	}

	public NodoDoble<T> darUltimo()
	{
		return ultimo;
	}
	
	public NodoDoble<T> darNodoPosicion(int pos){

		if(actual != null){

			actual = primero;
			int i=1;
			if(pos == 1){return actual.darSiguiente();}
			while (i < pos){
				actual = actual.darSiguiente();
				i++;
			}
			return actual;
		}
		return null;
	}

	public NodoDoble<T> darActual(){
		return actual;
	}

	public boolean estaVacio()
	{
		return longitud == 0;
	}

	public Iterator<T> iterator() {

		return new Iterator<T>()
		{

			private NodoDoble<T> temp = primero;

			public boolean hasNext() {
				return temp != null;

			}

			@Override
			public T next() {

				T resultado = null;
				if(temp != null)
				{
					resultado = temp.darElem();
					temp = temp.darSiguiente();
				}

				return resultado;
			}
		};


	}

	@Override
	public void agregarElementoFinal(T elem) {

		NodoDoble<T> nuevo = new NodoDoble<T>();
		nuevo.establecerElem(elem);


		if(primero == null)
		{
			primero = nuevo;
			ultimo = primero;
			actual = primero;
			longitud++;
			return;

		}

		ultimo.establecerSiguiente(nuevo);
		nuevo.establecerAnterior(ultimo);
		ultimo = nuevo;
		longitud++;
	}


	public T darElemento(int pos) {

		if(actual != null)
		{
			actual = primero;
			int i = 1;
			while(i < pos)
			{

				actual = actual.darSiguiente();
				i++;
			}
			return actual.darElem();
		}

		return null;
		
	}


	@Override
	public int darNumeroElementos() {

		return longitud;
	}

	@Override
	public T darElementoPosicionActual() {
		return actual.darElem();

	}

	@Override
	public boolean avanzarSiguientePosicion() {

		if(actual == ultimo)
		{
			return false;
		}
		else
		{
			actual = actual.darSiguiente();
			return true;
		}

	}

	@Override
	public boolean retrocederPosicionAnterior() {

		if(actual == primero)
		{
			return false;
		}
		else
		{
			actual = actual.darAnterior();
			return true;
		}
	}
	
	
	
	

}
