package data_structures;

import api.IQueue;

public class Queue<T> implements IQueue<T> {

	private ListaEncadenada<T> list;
	
	public Queue(){
		list = new ListaEncadenada<T>();
	}
	
	public  void enqueue(T item){
		list.enqueue(item);
	}
	
	public T dequeue(){
		return list.dequeue();
	}
	
	public boolean isEmpty(){
		return list.isEmpty();
	}
	
	public int size(){
		return list.darNumeroElementos();
	}
	
}
