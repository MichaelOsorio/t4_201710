package model.vo;

import data_structures.ILista;

public class VOPelicula implements Comparable<VOPelicula>{
	
	private String titulo;
	private int agnoPublicacion;
	private ILista<String> generosAsociados;
	
	public String getTitulo() {
		return titulo;
	}
	public void setTitulo(String titulo) {
		this.titulo = titulo;
	}
	
	public int getAgnoPublicacion() {
		return agnoPublicacion;
	}
	public void setAgnoPublicacion(int agnoPublicacion) {
		this.agnoPublicacion = agnoPublicacion;
	}
	
	public ILista<String> getGenerosAsociados() {
		return generosAsociados;
	}
	public void setGenerosAsociados(ILista<String> generosAsociados) {
		this.generosAsociados = generosAsociados;
	}
	
	public int compareTo(VOPelicula a) {
		// TODO Auto-generated method stub
		VOPelicula peli = a;
		if(titulo.compareToIgnoreCase(peli.getTitulo()) <= -1)
			{return -1;}
		else if(titulo.compareToIgnoreCase(peli.getTitulo()) >= 1){return 1;}
		else return 0;
	}
	
	
	
	

}
