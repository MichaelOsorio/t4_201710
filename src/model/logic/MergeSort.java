package model.logic;

public class MergeSort <T extends Comparable<T>>{
	
	private T aux[];
	
	public void sort(T[] a)
	{
		
		sort(a,0,a.length - 1);
	}

	
	public void sort(T[] a, int lo, int hi)
	{
		if(hi <= lo) return;
		int mid = lo + (hi - lo)/2;
		sort(a, lo, mid);
		sort(a, mid+1, hi);
		merge(a, lo, mid, hi);
	}
	
	public void merge(T[] a, int lo, int mid, int hi)
	{
		int i = lo;
		int j = mid+1;
		
		for(int k = lo; k <= hi; k++)
			aux[k] = a[k];
		
		for(int k = lo; k <= hi; k++)
			if(i < mid)
				a[k] = aux[j++];
			else if(j > hi)
				a[k] = aux[i++];
			else if(less(aux[j], aux[i]))
				a[k] = aux[j++];
			else
				a[k] = aux[i++];
	}


	private boolean less(T t, T t2) {
		
		return (t.compareTo(t2)<0);
	}
	
	public T[] resultado()
	{
		return aux;
	}


	



}
