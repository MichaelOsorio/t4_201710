package model.logic;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import java.util.Random;

import api.IManejadorPruebas;
import data_structures.ILista;
import data_structures.ListaDobleEncadenada;
import data_structures.ListaEncadenada;
import model.vo.VOPelicula;


public class MuestrasAleatorias implements IManejadorPruebas {

	private QuickSort quick;
	private QuickSortLista quickLista;

	private  ILista<VOPelicula> peliculas;


	private long tiempoEjecucion;


	public void cargarArchivoPeliculas(String archivoPeliculas)
	{
		peliculas = new ListaDobleEncadenada<VOPelicula>(); 

		try{
			BufferedReader br = new BufferedReader(new FileReader(archivoPeliculas));

			String line = br.readLine();

			while(line!= null){

				VOPelicula temp = new VOPelicula();
				String data = line;
				temp.setAgnoPublicacion(sacarAgno(data));
				String[] dataSplit = data.split(","); 
				temp.setTitulo(sacarNombre(dataSplit));
				temp.setGenerosAsociados(sacarGeneros(dataSplit));
				peliculas.agregarElementoFinal(temp);
				line = br.readLine();
			}
			br.close();

		}
		catch(IOException e){
			e.printStackTrace();
		}
		quick = new QuickSort();
		quickLista = new QuickSortLista();
	}

	private ILista<String> sacarGeneros(String[] dataSplit){
		ILista<String> generos = new ListaEncadenada<String>();

		String[] generosSplit = dataSplit[dataSplit.length-1].split("[|]");
		for(String tempo: generosSplit){
			generos.agregarElementoFinal(tempo);
		}

		return generos;
	}

	private String sacarNombre(String[] dataSplit){
		String nombre1 = "";
		String nombre = "";
		for(int i = 1; i < dataSplit.length-1; i++){

			nombre1 =  dataSplit[i];
			nombre += nombre1.replaceAll("\"", "");
		}

		return nombre;
	}

	private int sacarAgno(String data){

		String[] arregloAgno = data.split("[(]");
		int agno =0;

		for(int i =1; i < arregloAgno.length && agno ==0; i++){

			String temp = arregloAgno[arregloAgno.length-i].split("[)]")[0];
			if(isNumber(temp))
				agno = Integer.parseInt(temp.split("-")[0]);
		}


		return agno;

	}

	private boolean isNumber(String dato){
		try{
			Double d = Double.parseDouble(dato);
		}
		catch(NumberFormatException e){
			String agno= dato.split("-")[0];
			try{
				Double dou = Double.parseDouble(agno);

			}
			catch(NumberFormatException e2)
			{
				return false;
			}
			return true;
		}
		return true;
	}

	private VOPelicula[] generarMuestras(int n)
	{
		VOPelicula[] listaAux = new VOPelicula[n];

		Random rdm = new Random();
		int cantidadPeliculas = peliculas.darNumeroElementos();
		int pos =0;
		VOPelicula com = null;
		for(int i = 0; i < listaAux.length; i++)
		{
			pos = rdm.nextInt(cantidadPeliculas);
			com = peliculas.darElemento(pos);
			listaAux[i] = com;
		}

		return listaAux;

	}




	public void ordenarMuestraArreglo(int n)
	{

		Long pre  = System.currentTimeMillis();
		quick.sort(generarMuestras(n));
		Long  pos = System.currentTimeMillis();
		tiempoEjecucion = pos - pre;

		System.out.println("Tiempo de ejecución: " + tiempoEjecucion());

	}


	private ILista<VOPelicula> generarMuestraLista(int n){
		ILista<VOPelicula> listaAux = new ListaEncadenada<VOPelicula>();
		Random rdm = new Random();
		VOPelicula com = null;
		int pos =0;
		int cantidadPeliculas = peliculas.darNumeroElementos();
		for(int i = 0; i < n; i++){
			pos = rdm.nextInt(cantidadPeliculas);
			com = peliculas.darElemento(pos);
			listaAux.agregarElementoFinal(com);
		}

		return listaAux;

	}
	
	public void ordenarMuestraLista(int n){
		Long pre = System.currentTimeMillis();
		ListaEncadenada<VOPelicula> listaAux = (ListaEncadenada<VOPelicula>)generarMuestraLista(n);
		quickLista.sort(listaAux);
		Long pos = System.currentTimeMillis();
		tiempoEjecucion = pos - pre;
		
		System.out.println("Tiempo de ejecución :" + tiempoEjecucion());
		

	}

	public long tiempoEjecucion()
	{
		return tiempoEjecucion;
	}

}
