package model.logic;

import java.util.Random;

import data_structures.ListaDobleEncadenada;
import data_structures.ListaEncadenada;
import data_structures.NodoDoble;
import data_structures.NodoSencillo;
import model.vo.VOPelicula;

public class QuickSortLista {
	
	public void sort(ListaEncadenada<VOPelicula> lista){
		shuffle(lista);
		sort(lista,0, lista.darNumeroElementos());
	}

	private void sort(ListaEncadenada<VOPelicula> lista,int lo, int hi){
		if(hi<=lo)return;
		int j= partition(lista, lo, hi);
		sort(lista,lo,j-1);
		sort(lista, j+1, hi);
	}

	private int partition(ListaEncadenada<VOPelicula> lista, int lo, int hi){

		int i = lo, j = hi+1;

		while(true){
			while(less(lista.darElemento(++i), lista.darElemento(lo)))
				if(i == hi)break;

			while(less(lista.darElemento(lo),lista.darElemento(--j)))
				if(j==lo)break;

			if(i >= j) break;
			exch(lista,i,j);
		}

		exch(lista,lo,j);
		return j;
	}

	private boolean less(VOPelicula v , VOPelicula w){
		if(v.getTitulo().compareToIgnoreCase(w.getTitulo()) <= -1){return true;}else{return false;}
	}

	private void exch(ListaEncadenada<VOPelicula> lista,int i, int j ){

		NodoSencillo<VOPelicula> posI =lista.darNodoPosicion(i);
		NodoSencillo<VOPelicula> posJ = lista.darNodoPosicion(j);

		VOPelicula item1 = posI.darItem();
		posI.establecerItem(posJ.darItem());
		posJ.establecerItem(item1);
	}


	public  void shuffle(ListaEncadenada<VOPelicula> lista){

		Random rdm = new Random();

		NodoSencillo<VOPelicula> temp1 =null;
		NodoSencillo<VOPelicula> temp2 = null;

		VOPelicula item2 = null;
		int n = lista.darNumeroElementos();
		for(int i =0; i < lista.darNumeroElementos(); i++){

			int pos = rdm.nextInt(n);
			int pos2 = rdm.nextInt(n);

			temp1 = lista.darNodoPosicion(pos);
			temp2 = lista.darNodoPosicion(pos2);

			item2 = temp2.darItem();

			temp2.establecerItem(temp1.darItem());
			temp1.establecerItem(item2);


		}

	}
}
