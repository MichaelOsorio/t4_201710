package model.logic;

import java.util.Random;

import model.vo.VOPelicula;

public class QuickSort {

	private int partition(VOPelicula[] a, int lo, int hi){
		int i = lo, j = hi+1;


		while(true)
		{
			while(less(a[++i], a[lo])){
				if(i == hi) break;
			}
			while(less(a[lo], a[--j])){
				if(j == lo) break;
			}
			if( i >= j) break;
			exch(a,i,j);
		}

		exch(a, lo, j);
		return j;
	}

	public void sort(VOPelicula[] a){
		shuffel(a);
		sort(a, 0, a.length-1);
	}
	
	private void sort(VOPelicula[] a, int lo, int hi){
		if(hi <= lo)return;
		int j = partition(a,lo,hi);
		sort(a, lo, j-1);
		sort(a, j+1,hi);

	}
	
	private boolean less (VOPelicula a,VOPelicula b){
		return a.compareTo(b) <0;
	}
	
	private void exch(VOPelicula[] a, int i, int j){
		VOPelicula t = a[i]; a[i] = a[j]; a[j] = t;
	}
	
	private void shuffel(VOPelicula[] lista){
		
		
		Random rnd = new Random();
		if(lista != null){
			for(int i =0; i < lista.length; i++)
			{
				int pos1 = rnd.nextInt(lista.length);
				int pos2 = rnd.nextInt(lista.length);

				VOPelicula a = lista[pos1];
				VOPelicula b = lista[pos2];

				lista[pos2] = a;
				lista[pos1] =b;
			}
		}
		
	}
}
