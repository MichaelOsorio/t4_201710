package view;

import java.util.Scanner;

import model.vo.VOPelicula;
import controller.Controller;

public class VistaManejadorPruebas {

	public static void main(String[] args) {


		Scanner sc=new Scanner(System.in);
		boolean fin=false;
		while(!fin){
			printMenu();

			int option = sc.nextInt();

			switch(option){
			case 1:
				Controller.cargarArchivoPeliculas();
				break;
			case 2:
				System.out.println("Ingrese el numero de elementos que llevar� la estructura para la prueba:");
				int numeroDatos=sc.nextInt();
				boolean reProbar = true;
				int numeroRep =1;
				while(reProbar){
					System.out.println("1. lista \n 2. Arreglo");
					int tipoEstrutura = sc.nextInt();

					switch(tipoEstrutura){
					case 1:

						Controller.ordenarMuestrasLista(numeroDatos);
						break;
					case 2: Controller.ordenarMuestrasArreglo(numeroDatos);
					break;
					}
					System.out.println("Numero de ejecuciones: "+ numeroRep);
					numeroRep++;
					System.out.println("Repetir prueba con la misma cantidad de datos? :\n 1. S� \n 2. No");
					int repetir = sc.nextInt();
					if(repetir == 2){reProbar = false; numeroRep = 1;}
				}
				break;

			case 3:	
				fin=true;
				break;
			}


		}
	}

	private static void printMenu() {
		System.out.println("---------ISIS 1206 - Estructuras de datos----------");
		System.out.println("---------------------Taller 4----------------------");
		System.out.println("1. Cree una nueva colecci�n de pel�culas (data/movies.csv)");
		System.out.println("2. Iniciar prueba");
		System.out.println("3. Salir");

	}

}
